default:
    @just --list

export SGE_VERSION := "1.0.7"
export SGE_RISK_VERSION := "1.0.7"
export SGE_ALPHABETAAGENT_VERSION := "1.0.4"
export SGE_MCTSAGENT_VERSION := "1.0.4"
export SGE_RANDOMAGENT_VERSION := "1.0.4"
export RISKAGENT_VERSION := "1.0.4"

zip:
    zip -9 pss-risk.zip sge-{{ SGE_VERSION }}-exe.jar sge-{{ SGE_VERSION }}.jar sge-{{ SGE_VERSION }}-sources.jar sge-{{ SGE_VERSION }}-javadoc.jar sge-risk-{{ SGE_RISK_VERSION }}-exe.jar sge-risk-{{ SGE_RISK_VERSION }}.jar sge-risk-{{ SGE_RISK_VERSION }}-sources.jar sge-risk-{{ SGE_RISK_VERSION }}-javadoc.jar agents/sge-alphabetaagent-{{ SGE_ALPHABETAAGENT_VERSION }}.jar agents/sge-mctsagent-{{ SGE_MCTSAGENT_VERSION }}.jar agents/sge-randomagent-{{ SGE_RANDOMAGENT_VERSION }}.jar agents/RiskAgent-{{ RISKAGENT_VERSION }}.jar readme/SGE-MANUAL.pdf readme/RISK_AGENT_GUIDE.pdf readme/RULESET.pdf boards/risk_default.yaml boards/risk_simple_3.yaml

dist: dist-engine dist-agents dist-readme dist-boards
    
dist-engine:
    cp strategy-game-engine/build/libs/sge-{{ SGE_VERSION }}-exe.jar strategy-game-engine/build/libs/sge-{{ SGE_VERSION }}.jar strategy-game-engine/build/libs/sge-{{ SGE_VERSION }}-sources.jar strategy-game-engine/build/libs/sge-{{ SGE_VERSION }}-javadoc.jar . 
    cp sge-risk/build/libs/sge-risk-{{ SGE_RISK_VERSION }}-exe.jar sge-risk/build/libs/sge-risk-{{ SGE_RISK_VERSION }}.jar sge-risk/build/libs/sge-risk-{{ SGE_RISK_VERSION }}-sources.jar sge-risk/build/libs/sge-risk-{{ SGE_RISK_VERSION }}-javadoc.jar . 

dist-agents:
    mkdir -p agents/
    cp sge-alphabetaagent/build/libs/sge-alphabetaagent-{{ SGE_ALPHABETAAGENT_VERSION }}.jar agents/
    cp sge-mctsagent/build/libs/sge-mctsagent-{{ SGE_MCTSAGENT_VERSION }}.jar agents/
    cp sge-randomagent/build/libs/sge-randomagent-{{ SGE_RANDOMAGENT_VERSION }}.jar agents/
    cp RiskAgent/build/libs/RiskAgent-{{ RISKAGENT_VERSION }}.jar agents/

dist-readme:
    mkdir -p readme/
    cp strategy-game-engine/manual/SGE-MANUAL.pdf readme/
    cp sge-risk/manual/RISK_AGENT_GUIDE.pdf readme/
    cp sge-risk/manual/RULESET.pdf readme/

dist-boards:
    mkdir -p boards/
    cp sge-risk/maps/risk_default.yaml boards/
    cp sge-risk/maps/risk_simple_3.yaml boards/
    

build:
    just build-exe-submodule strategy-game-engine sge {{ SGE_VERSION }}
    just build-exe-submodule sge-risk sge-risk {{ SGE_RISK_VERSION }}
    just build-submodule sge-alphabetaagent sge-alphabetaagent {{ SGE_ALPHABETAAGENT_VERSION }}
    just build-submodule sge-mctsagent sge-mctsagent {{ SGE_MCTSAGENT_VERSION }}
    just build-submodule sge-randomagent sge-randomagent {{ SGE_RANDOMAGENT_VERSION }}
    just build-submodule RiskAgent RiskAgent {{ RISKAGENT_VERSION }}

build-exe-submodule SUBMODULE PROJECT VERSION:
    cd {{ SUBMODULE }}; git checkout v{{ VERSION }}
    cd {{ SUBMODULE}}; ./gradlew jar shadowJar sourcesJar javadocJar; just build

build-submodule SUBMODULE PROJECT VERSION:
    cd {{ SUBMODULE }}; git checkout v{{ VERSION }}
    cd {{ SUBMODULE}}; ./gradlew jar

update:
    git submodule update --init
    just update-submodule strategy-game-engine {{ SGE_VERSION }}
    just update-submodule sge-risk {{ SGE_RISK_VERSION }}
    just update-submodule sge-alphabetaagent {{ SGE_ALPHABETAAGENT_VERSION }}
    just update-submodule sge-mctsagent {{ SGE_MCTSAGENT_VERSION }}
    just update-submodule sge-randomagent {{ SGE_RANDOMAGENT_VERSION }}
    just update-submodule RiskAgent {{ RISKAGENT_VERSION }}

update-submodule SUBMODULE VERSION:
    cd {{SUBMODULE}}; git fetch --all
    cd {{SUBMODULE}}; git switch main
    cd {{SUBMODULE}}; git pull --stat --ff-only || git pull --stat --rebase
    cd {{SUBMODULE}}; git checkout v{{VERSION}}

clean:
    rm -f pss-risk.zip
    rm -rf agents/
    rm -rf boards/
    rm -rf readme/
    rm -f *.jar
